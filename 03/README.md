# **Advent Of Code**

**❗Language: Java**

## Day 3
  * [Part 1](https://adventofcode.com/2022/day/3#part1)
  * [Part 2](https://adventofcode.com/2022/day/3#part2)
  * [Solution](aoc.java)

```bash
$ java aoc.java
```
