import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class Main {
  public static int points = 0;

  public static int calculate_priority(char id) {
    int priority = 0;
    if (Character.isUpperCase(id)) {
      priority = ((int)id - (int)'A' + 27);
    } else {
      priority = ((int)id - (int)'a' + 1);
    }
    return priority;
  }

  public static void calculate_part1(String data) {
    int full_size = data.length();
    int compartment_size = (full_size / 2);
    boolean found = false;

    for (int i = 0; i < compartment_size && !found; ++i) {
      for (int j = compartment_size; j < full_size && !found; ++j) {
        char di = data.charAt(i);
        char dj = data.charAt(j);
        if (di == dj) {
          found = true;
          int priority = calculate_priority(di);
          points += priority;
          System.out.println("{" + di + ", " + priority + "}: " + points);
        }
      }
    }
  }

  public static void calculate_part2(String l1, String l2, String l3) {
    boolean found = false;

    for (int i = 0; i < l1.length() && !found; ++i) {
      for (int j = 0; j < l2.length() && !found; ++j) {
        for (int k = 0; k < l3.length() && !found; ++k) {
          char di = l1.charAt(i);
          char dj = l2.charAt(j);
          char dk = l3.charAt(k);
          if (di == dj && di == dk) {
            found = true;
            int priority = calculate_priority(di);
            points += priority;
            System.out.println("{" + di + ", " + priority + "}: " + points);
          }
        }
      }
    }
  }

  public static String get_next_line(Scanner scanner) {
    if (scanner.hasNextLine()) {
      return scanner.nextLine();
    }
    return "";
  }

  public static void read_file(String file) {
    try {
      File myObj = new File(file);
      Scanner myReader = new Scanner(myObj);
      while (myReader.hasNextLine()) {
        String line1 = get_next_line(myReader);
        String line2 = get_next_line(myReader);
        String line3 = get_next_line(myReader);
        //calculate_part1(line1);
        //calculate_part1(line2);
        //calculate_part1(line3);
        calculate_part2(line1, line2, line3);
      }
      myReader.close();
    } catch (FileNotFoundException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    read_file("input");
  }
}
