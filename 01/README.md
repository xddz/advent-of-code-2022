# **Advent Of Code**

**❗Language: Python**

## Day 1
  * [Part 1](https://adventofcode.com/2022/day/1#part1)
  * [Part 2](https://adventofcode.com/2022/day/1#part2)
  * [Solution](aoc.py)

