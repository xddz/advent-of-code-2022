#!/bin/python3

list = []
if __name__ == '__main__':
    value = 0
    with open("input", "r") as f:
        for line in f.readlines():
            if line == "\n":
                list.append(value)
                value = 0
                continue
            value += int(line)
        list.sort()
        print(list)
        print("Biggest total calories: `" + str(list[-1]) + "`")
        print("Sum of top 3 biggest total calories: `" + str(list[-1] + list[-2] + list[-3]) + "`")
