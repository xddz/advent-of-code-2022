use std::fs;

// edit this constant (1/2)
const SOLUTION_PART : u8 = 2;

fn win(points : &mut u64 , score : u64) {
    *points += score;
    println!("Win! {}", points);
}

fn tie(points : &mut u64 , score : u64) {
    *points += score;
    println!("Tie! {}", points);
}

fn loss(points : &mut u64 , score : u64) {
    *points += score;
    println!("Loss! {}", points);
}

fn main() {
    let contents = fs::read_to_string("input")
        .expect("Should have been able to read the file");

    let mut points : u64 = 0;
    for line in contents.lines() {
        let opponent : char = line.as_bytes()[0] as char;
        let me       : char = line.as_bytes()[2] as char;
        if SOLUTION_PART == 1 {
            match opponent {
                'A' => match me {
                            'X' => tie(&mut points,  1 + 3),
                            'Y' => win(&mut points,  2 + 6),
                            'Z' => loss(&mut points, 3 + 0),
                             _  => println!("Unknown! {}", points),
                       },
                'B' => match me {
                            'X' => loss(&mut points, 1 + 0),
                            'Y' => tie(&mut points,  2 + 3),
                            'Z' => win(&mut points,  3 + 6),
                             _  => println!("Unknown! {}", points),
                       },
                'C' => match me {
                            'X' => win(&mut points,  1 + 6),
                            'Y' => loss(&mut points, 2 + 0),
                            'Z' => tie(&mut points,  3 + 3),
                             _  => println!("Unknown! {}", points),
                       },
                 _  => println!("opponent played unknown"),
            };
        } else if SOLUTION_PART == 2 {
            match opponent {
                'A' => match me {
                            'X' => loss(&mut points, 3 + 0),
                            'Y' => tie(&mut points,  1 + 3),
                            'Z' => win(&mut points,  2 + 6),
                             _  => println!("Unknown! {}", points),
                       },
                'B' => match me {
                            'X' => loss(&mut points, 1 + 0),
                            'Y' => tie(&mut points,  2 + 3),
                            'Z' => win(&mut points,  3 + 6),
                             _  => println!("Unknown! {}", points),
                       },
                'C' => match me {
                            'X' => loss(&mut points, 2 + 0),
                            'Y' => tie(&mut points,  3 + 3),
                            'Z' => win(&mut points,  1 + 6),
                             _  => println!("Unknown! {}", points),
                       },
                 _  => println!("opponent played unknown"),
            };
        }
    }
    println!("Total: {}", points);
}
