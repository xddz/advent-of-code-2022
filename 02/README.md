# **Advent Of Code**

**❗Language: Rust**

## Day 2
  * [Part 1](https://adventofcode.com/2022/day/2#part1)
  * [Part 2](https://adventofcode.com/2022/day/2#part2)
  * [Solution](aoc.rs)

```bash
$ rustc aoc.rs -o aoc
```
