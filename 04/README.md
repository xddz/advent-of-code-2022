# **Advent Of Code**

**❗Language: C**

## Day 4
  * [Part 1](https://adventofcode.com/2022/day/4#part1)
  * [Part 2](https://adventofcode.com/2022/day/4#part2)
  * [Solution](aoc.c)

```bash
$ gcc aoc.c -Ofast -o aoc
$ ./aoc
```
