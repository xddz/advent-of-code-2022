#include <stdio.h>
#include <stdlib.h>

/* edit this! */
#define CHALLENGE_PART (2)

char * read_file(const char * const restrict fname, size_t * length) {
  FILE * f = fopen(fname, "rb");
  if (!f) {
    return (char *)NULL;
  }

  fseek(f, 0, SEEK_END);
  *length = ftell(f);
  fseek(f, 0, SEEK_SET);
  if (!*length || *length > 1073741824) {
    fclose(f);
    return (char *)NULL;
  }

  char * contents = malloc(sizeof(char) * (*length + 1));
  size_t read_length = fread(contents, 1, *length, f);

  if (*length != read_length) {
    free(contents);
    fclose(f);
    return (char *)NULL;
  }

  fclose(f);
  contents[*length] = 0;
  return contents;
}

size_t find_next(char * str, char d) {
  char * s = str;
  size_t ssz = 0;
  while (*s && *s != d) {
    ++ssz;
    ++s;
  }
  return ssz;
}

int main(void) {
  size_t file_size = 0;
  char * file = read_file("input", &file_size);
  if (!file) {
    return -1;
  }

  size_t points = 0;
  for (char * f = file; *f; ++f) {
    size_t next_minus = find_next(f, '-');
    char * first = f;
    first[next_minus] = 0;
    f += next_minus + 1;

    size_t next_comma = find_next(f, ',');
    char * second = f;
    second[next_comma] = 0;
    f += next_comma + 1;

    next_minus = find_next(f, '-');
    char * third = f;
    third[next_minus] = 0;
    f += next_minus + 1;

    const size_t next_eol = find_next(f, '\n');
    char * fourth = f;
    fourth[next_eol] = 0;
    f += next_eol;

    size_t n_first  = atoi(first);
    size_t n_second = atoi(second);
    size_t n_third  = atoi(third);
    size_t n_fourth = atoi(fourth);

#if CHALLENGE_PART == 1
    if ((n_first <= n_third && n_second >= n_fourth) || (n_third <= n_first && n_fourth >= n_second)) {
      ++points;
    }
#else
    if ((n_first <= n_third && n_second >= n_fourth) || (n_third <= n_first && n_fourth >= n_second) || 
        (n_first == n_third) || (n_second == n_third) || (n_first == n_fourth) || (n_second == n_fourth) ||
        (n_first < n_third && n_third < n_second) || (n_first < n_fourth && n_fourth < n_second)) {
      ++points;
    }
#endif

    printf("(%d) = first: [%d-%d], second: [%d-%d]\n", points, n_first, n_second, n_third, n_fourth);
  }

  free(file);
}
